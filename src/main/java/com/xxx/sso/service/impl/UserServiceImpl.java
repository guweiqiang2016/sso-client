package com.xxx.sso.service.impl;

import com.xxx.sso.dao.UserDao;
import com.xxx.sso.entity.User;
import com.xxx.sso.service.UserService;

import java.util.Set;

/**
 * user service impl
 */
public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	private PasswordHelper passwordHelper;

	public void setPasswordHelper(PasswordHelper passwordHelper) {
		this.passwordHelper = passwordHelper;
	}

	public User createUser(User user) {
		passwordHelper.encryptPassword(user); // 加密密码
		return userDao.createUser(user);
	}

	public void changePassword(Long userId, String newPassword) {
		User user = userDao.findOne(userId);
		user.setPassword(newPassword);
		passwordHelper.encryptPassword(user);
		userDao.updateUser(user);
	}

	public void correlationRoles(Long userId, Long... roleIds) {
		userDao.correlationRoles(userId, roleIds);
	}

	public void uncorrelationRoles(Long userId, Long... roleIds) {
		userDao.uncorrelationRoles(userId, roleIds);
	}

	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public Set<String> findRoles(String username) {
		return userDao.findRoles(username);
	}

	public Set<String> findPermissions(String username) {
		return userDao.findPermissions(username);
	}

}
